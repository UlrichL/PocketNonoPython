from django.apps import AppConfig


class NonodjoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nonodjo'
