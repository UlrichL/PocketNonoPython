from nonodjo.light_models import GameState


class GameStateService:
    def __init__(self):
        self.game_state = GameState()

    def get(self):
        return self.game_state

    def change_size(self, row_count, column_count):
        self.game_state = self.game_state.copy_with(row_count, column_count)
