from django.urls import path
from nonodjo.game_state_service import GameStateService
from nonodjo.service_locator import service_locator
from nonodjo.views import GamePageView, InteractionView

# This is a sort of entry point: prepare the service locator
service_locator.register('game_state_service', GameStateService())

urlpatterns = [
    path("", GamePageView.as_view(), name="game"),
    path("click", InteractionView.as_view()),
    path("change", GamePageView.as_view()),
]
