from datetime import datetime
from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView

from nonodjo.game_state_service import GameStateService
from nonodjo.light_models import GameState
from nonodjo.service_locator import service_locator


class ChangeSizeForm(forms.Form):
    row_count = forms.IntegerField(
        label="Rows",
        validators=[MinValueValidator(1), MaxValueValidator(20)]
    )
    column_count = forms.IntegerField(
        label="Columns",
        validators=[MinValueValidator(1), MaxValueValidator(40)]
    )


# This view class has two functions: basic page rendering and processing (and managing) a form
class GamePageView(FormView):
    template_name = "nonodjo/game.html"
    form_class = ChangeSizeForm
    success_url = "/"

    def get_initial(self):
        initial = super().get_initial()

        game_state: GameState = service_locator.get('game_state_service').get()
        initial['row_count'] = game_state.row_count
        initial['column_count'] = game_state.column_count

        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        game_state: GameState = service_locator.get('game_state_service').get()
        context["game"] = game_state

        return context

    def form_valid(self, form: ChangeSizeForm):
        # This method is called when valid form data has been POSTed.

        game_state_service: GameStateService = service_locator.get('game_state_service')
        game_state_service.change_size(form.cleaned_data["row_count"], form.cleaned_data["column_count"])

        return super().form_valid(form)


class InteractionView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(InteractionView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        column = int(request.POST.get('x', '0'))
        row = int(request.POST.get('y', '0'))

        if column == 0 or row == 0:
            return HttpResponseBadRequest('expected x and y values')

        game_state: GameState = service_locator.get('game_state_service').get()
        game_state.click(row, column)

        return HttpResponse(content_type='text/plain', content=str(game_state.count_activated_fields()))
