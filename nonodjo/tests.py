from django.test import TestCase

from nonodjo.game_state_service import GameStateService
from nonodjo.light_models import GameState


class GameStateTestCase(TestCase):
    def test_a_new_state_has_no_active_fields(self):
        game_state = GameState()
        self.assertEqual(game_state.count_activated_fields(), 0)

    def test_a_field_can_be_activated(self):
        game_state = GameState()
        self.assertEqual(game_state.fields[0][0], False)
        game_state.click(1, 1)
        self.assertEqual(game_state.fields[0][0], True)
        self.assertEqual(game_state.count_activated_fields(), 1)

    def test_a_field_can_only_be_activated_once(self):
        game_state = GameState()
        self.assertEqual(game_state.fields[0][0], False)
        game_state.click(1, 1)
        game_state.click(1, 1)
        self.assertEqual(game_state.fields[0][0], False)
        self.assertEqual(game_state.count_activated_fields(), 0)


class GameStateServiceTestCase(TestCase):
    def test_it_returns_the_same_state(self):
        game_state_service = GameStateService()
        self.assertEqual(game_state_service.get(), game_state_service.get())

    def test_it_can_change_the_board_size(self):
        game_state_service = GameStateService()
        game_state_service.change_size(1, 13)
        game_state = game_state_service.get()
        self.assertEqual(game_state.row_count, 1)
        self.assertEqual(game_state.column_count, 13)

    def test_it_preserves_board_state(self):
        game_state_service = GameStateService()
        game_state1 = game_state_service.get()
        self.assertEqual(game_state1.fields[0][3], False)
        self.assertEqual(game_state1.fields[0][2], False)

        game_state1.fields[0][3] = True
        game_state_service.change_size(1, 11)

        game_state2 = game_state_service.get()
        self.assertEqual(game_state2.fields[0][3], True)
        self.assertEqual(game_state2.fields[0][2], False)
