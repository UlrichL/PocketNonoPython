# A rather basic locator class for dependency "injection" of services
class ServiceLocator:
    def __init__(self):
        self.__services = {}

    def register(self, name, service_class):
        self.__services[name] = service_class

    def get(self, name):
        # Maybe add some error handling or fallbacks
        return self.__services[name]


service_locator = ServiceLocator()
