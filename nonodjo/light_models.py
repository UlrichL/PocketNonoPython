# Models which are not persisted in DB yet

class GameState:
    def __init__(self, rows=3, cols=6):
        self.row_count = rows
        self.column_count = cols
        self.fields = [[False for x in range(self.column_count)] for y in range(self.row_count)]

    def count_activated_fields(self):
        count = 0

        for line in self.fields:
            for column in line:
                if column: count = count+1

        return count

    def click(self, row, column):
        self.fields[row-1][column-1] = not self.fields[row-1][column-1]

    def copy_with(self, row_count, column_count):
        new_game_state = GameState(row_count, column_count)

        # Preserve the old board state as much as possible
        for row in range(min(self.row_count, row_count)):
            for column in range(min(self.column_count, column_count)):
                new_game_state.fields[row][column] = self.fields[row][column]

        return new_game_state

    def __str__(self):
        parts = map(lambda line: '[{}]'.format(','.join(map(lambda column: str(column), line))), self.fields)

        return '[{}x{}: {}]'.format(self.row_count, self.column_count, ', '.join(parts))
